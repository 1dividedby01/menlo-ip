import torch
from torch import nn
from torch.utils.data import DataLoader, TensorDataset
import pandas

import pandas as pd
import numpy as np
import nltk

mpqa_csv = pd.read_csv('mpqa_data.csv')
csv = pd.read_csv('CNN_training.csv')

from collections import Counter

from string import punctuation

from nltk.tag import pos_tag
from nltk.corpus import words
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

words_set = set(words.words())

ps = PorterStemmer()
lemmatizer = WordNetLemmatizer()
counter = 0

def preprocess(text):
    global counter
    counter+=1
    if counter % 10000 == 0:
        print(counter)
    processed = text.replace("-", " ").replace("/", " ")
    processed = ' '.join(processed.split())
    processed = processed.strip()
    processed = processed.replace("\'s", "").replace(" s ", " ")
    processed = ''.join([c for c in processed if c not in punctuation])
    comps = processed.split()
    tags = pos_tag(comps)
    propernouns = [word for word,pos in tags if pos == 'NNP']
    verbs = [word for word,pos in tags if len(pos) == 3 and 'VB' in pos]
    numerals = [word for word,pos in tags if pos == 'CD']
    properadj = [word for word,pos in tags if pos == 'JJ' and word != tags[0][0] and word.lower() != word]
    # print(propernouns)
    # print(verbs)
    for i in range(len(comps)):
        word = comps[i]

        if "$" in word or "£" in word or "€" in word:
            comps[i] = "<money>"
        elif "%" in word or (i+1 < len(comps) and comps[i+1].lower() == "percent"):
            comps[i] = "<percentage>"
        elif word.isdigit() and int(word) >= 1900 and int(word) <= 2075:
            comps[i] = "<year>"
        elif word in numerals:
            comps[i] = "<number>"
        elif word in propernouns:
            comps[i] = "<propernoun>"
        elif word in properadj:
            comps[i] = "<properadj>"
        else:
            lemm = lemmatizer.lemmatize(comps[i])
            if comps[i] in verbs:
                comps[i] = lemmatizer.lemmatize(comps[i], pos='v')
                continue
            # stem = ps.stem(comps[i])
            if lemm in words_set:
                comps[i] = lemm
            elif lemm.lower() in words_set:
                comps[i] = lemm.lower()
            else:
                comps[i] = comps[i].lower()
                # print(lemm + ", " + comps[i])

    processed = ' '.join(comps)
    return processed.lower()

# csv = pd.read_csv('menlo-ip/training_data.csv')

# csv['subjective'] = np.where(csv['subjective'] == True, 1, 0)

# csv['sentence'] = csv['sentence'].str.lower()

# should be around 26 for CNN data
sequence_length = 35

# def remove_punctuation(text):
#   return ''.join([c for c in text if c not in punctuation])

# csv['sentence'] = csv['sentence'].apply(remove_punctuation)

cat = mpqa_csv['sentence'].str.cat(sep=' ')

words = cat.split()

counts = Counter(words)

total = len(counts)
sorted_words = counts.most_common(total)

vocab_dict = {w:i+1 for i, (w,c) in enumerate(sorted_words)}

def to_ints(row):
    if not isinstance(row, str):
        return []
    return [vocab_dict[w] for w in row.split() if w in vocab_dict.keys()]

csv['sentence'] = csv['sentence'].apply(to_ints)

csv['sentence'] = csv['sentence'].convert_objects(convert_numeric=True)

print(len(vocab_dict))
print(csv['sentence'].apply(len).describe())
# print(csv['sentence'].convert_objects(convert_numeric=True).head())
csv = csv[csv['sentence'].map(len) > 4]

def pad_features(sentence):
  if len(sentence) < sequence_length:
    zeros = list(np.zeros(sequence_length-len(sentence)))
    return zeros+list(sentence)
  else:
    return sentence[0:sequence_length]

csv['sentence'] = csv['sentence'].apply(pad_features)

# csv = csv.sample(frac=1).reset_index(drop=True)

train, validate, test = np.split(csv, [int(.8*len(csv)), int(.9*len(csv))])
# have to recompile these because pandas csv reading and numpy aren't always compatible
# print(np.array(train["sentence"].values.tolist()))
train_X = np.array(list(train["sentence"].values))
validate_X = np.array(validate["sentence"].values.tolist())
test_X = np.array(test["sentence"].values.tolist())
# print(train_X)
# print(np.array(train["sentence"].values.tolist()))

train_data = TensorDataset(torch.tensor(train_X), torch.tensor(train["subjective"].values))
validate_data = TensorDataset(torch.tensor(validate_X), torch.tensor(validate["subjective"].values))
test_data = TensorDataset(torch.tensor(test_X), torch.tensor(test["subjective"].values))

batch_size = 100

train_loader = DataLoader(train_data, shuffle=True, batch_size=batch_size)
validate_loader = DataLoader(validate_data, shuffle=True, batch_size=batch_size)
test_loader = DataLoader(test_data, shuffle=True, batch_size=batch_size)

train_on_gpu = False

class SubjectivityLSTM(nn.Module):
  def __init__(self, vocab_size, output_size, embedding_dim, hidden_dim, n_layers, drop_prob=0.5):
    super().__init__()

    self.output_size = output_size
    self.n_layers = n_layers
    self.hidden_dim = hidden_dim

    # embedding and LSTM layers
    self.embedding = nn.Embedding(vocab_size, embedding_dim)
    self.lstm = nn.LSTM(embedding_dim, hidden_dim // 2, n_layers,
                       dropout=drop_prob, batch_first=True, bidirectional=True)

    '''
    randomly dropout neurons from the network to increase responsibility among
    individual neurons and reduce dependency on other neurons. Dropout is a
    solution to coadaptation, a major cause of overfitting.
    '''

    # dropout layer
    self.dropout = nn.Dropout(drop_prob)

    # linear and sigmoid layers
    self.fc = nn.Linear(hidden_dim, output_size)
    self.sig = nn.Sigmoid()

  def forward(self, inputs, hidden):
    batch_size = inputs.size(0)
#     print(inputs)
    embeds = self.embedding(inputs)
    lstm_out, hidden = self.lstm(embeds, hidden)

    # contiguous just creates a contiguous copy in memory
    lstm_out = lstm_out.contiguous().view(-1, self.hidden_dim)

    # dropout and fully-connected layer
    out = self.dropout(lstm_out)
    out = self.fc(out)
    # sigmoid function
    sig_out = self.sig(out)

    # reshape to be batch_size first
    sig_out = sig_out.view(batch_size, -1)
    sig_out = sig_out[:, -1] # get last batch of labels

    # return last sigmoid output and hidden state
    return sig_out, hidden

  def init_hidden(self, batch_size):
    weight = next(self.parameters()).data

    if train_on_gpu:
      hidden = (weight.new(self.n_layers * 2, batch_size, self.hidden_dim // 2).zero_().cuda(),
               weight.new(self.n_layers * 2, batch_size, self.hidden_dim // 2).zero_().cuda())
    else:
      hidden = (weight.new(self.n_layers * 2, batch_size, self.hidden_dim // 2).zero_(),
               weight.new(self.n_layers * 2, batch_size, self.hidden_dim // 2).zero_())

    return hidden

def create(embedding_dim, hidden_dim):
  vocab_size = len(vocab_dict) + 1 # the 1 is for the 0 padding
  output_size = 1
  n_layers = 2

  net = SubjectivityLSTM(vocab_size, output_size, embedding_dim, hidden_dim, n_layers)

  return net

net = create(600, 100)

def load_model(name):
  path = name + ".pth.tar"
  checkpoint = torch.load(path)
  net.load_state_dict(checkpoint['state_dict'])
  start_batch = checkpoint['batch']
  start_epoch = checkpoint['epoch']

  return start_batch, start_epoch, net

start_batch, epoch, net = load_model("model_mpqa_updated")
# get accuracy on test data

test_losses = [] # track loss
num_correct = 0

# hidden layer
criterion = nn.BCELoss()
h = net.init_hidden(batch_size)

net.eval()
all_data = []
for inputs, labels in test_loader:

  h = tuple([each.data for each in h])

  if(train_on_gpu):
      inputs, labels = inputs.cuda(), labels.cuda()

  # get predicted outputs
  inputs = inputs.type(torch.LongTensor)

  if((inputs.shape[0],inputs.shape[1]) != (batch_size,sequence_length)):
    continue

  output, h = net(inputs, h)

  # calculate loss
  test_loss = criterion(output.squeeze(), labels.float())
  test_losses.append(test_loss.item())

  # convert output probabilities to predicted class (0 or 1)
  pred = torch.round(output.squeeze())  # rounds to the nearest integer

  # compare predictions to true label
  correct_tensor = pred.eq(labels.float().view_as(pred))
  num_within_range = 0
  num_correct_local = 0
  for i in range(len(correct_tensor)):
    all_data.append(output.squeeze()[i])
    print("Prediction: "+ str(output.squeeze()[i]) + ", Label: " + str(labels[i]))

    if output.squeeze()[i] < 0.075 or output.squeeze()[i] > 0.925:
      num_within_range += 1
      if correct_tensor[i] == 1:
        num_correct_local+=1

  # the accuracy is really high in the outskirts compared to the middle
  # print("Batch")
  # print(float(num_correct_local)/num_within_range)
  # print(num_within_range)
  # print(len(correct_tensor))
  correct = np.squeeze(correct_tensor.numpy()) if not train_on_gpu else np.squeeze(correct_tensor.cpu().numpy())
  num_correct += np.sum(correct)

print(np.percentile(np.array(all_data), 10))
print(np.percentile(np.array(all_data), 50))
print(np.percentile(np.array(all_data), 90))
print("Test loss: {:.3f}".format(np.mean(test_losses)))

# accuracy over all test data
test_acc = num_correct/len(test_loader.dataset)
print("Test accuracy: {:.3f}".format(test_acc))

from string import punctuation

def tokenize_review(test_review):
    test_review = test_review.lower() # lowercase
    # get rid of punctuation
    test_text = ''.join([c for c in test_review if c not in punctuation])

    # splitting by spaces
    test_words = test_text.split()

    # tokens
    test_ints = [vocab_dict[word] for word in test_words if word in vocab_dict.keys()]

    return test_ints

# # test code and generate tokenized review
# test_ints = tokenize_review(test_review)
# print(test_ints)


# # test sequence padding
# seq_length=200
# features = pad_features(test_ints)

# print(features)


# # test conversion to tensor and pass into your model
# feature_tensor = torch.FloatTensor(features)
# print(feature_tensor.size())


def predict(test_review, sequence_length=200):

    net.eval()

    # tokenize review
    test_ints = tokenize_review(test_review)

    # pad tokenized sequence
    seq_length=sequence_length
    features = pad_features(test_ints)
    features = [[int(i) for i in features]]
    # convert to tensor to pass into your model
    feature_tensor = torch.tensor(np.array(features))

    batch_size = feature_tensor.size(0)

    # initialize hidden state
    h = net.init_hidden(batch_size)

    if(train_on_gpu):
        feature_tensor = feature_tensor.cuda()

    # get the output from the model
    output, h = net(feature_tensor, h)
    # convert output probabilities to predicted class (0 or 1)
    pred = torch.round(output.squeeze())
    # printing output value, before rounding
#     print('Prediction value, pre-rounding: {:.6f}'.format(output.item()))

#     return pred.item()
    return output.item()

# print(predict(preprocess("The dog is really crazy")))

csv_new = pd.read_csv('CNN_training.csv')
accuracy = 0
for i in range(100):

  sentence = csv_new.iloc[i, :].sentence
  print(sentence)
  label = csv_new.iloc[i, :].subjective
  if round(predict(sentence)) == label:
    accuracy += 1

print(accuracy/100)
