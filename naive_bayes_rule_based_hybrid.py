import pandas as pd
import numpy as np
import nltk
from string import punctuation

csv_mpqa = pd.read_csv('MPQA/mpqa_train.csv')
csv_mpqa_validation = pd.read_csv('MPQA/mpqa_validate.csv')

from sklearn.model_selection import train_test_split

# X_train, X_test, y_train, y_test = train_test_split(csv_mpqa['sentence'], csv_mpqa['subjective'], train_size=0.9, random_state=0)
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import roc_auc_score

from sklearn.pipeline import Pipeline

text_clf = Pipeline([
     ('vect', CountVectorizer()),
     ('tfidf', TfidfTransformer()),
     ('clf', MultinomialNB(
                           alpha=1e-3))
 ])

# vect = CountVectorizer().fit(csv_mpqa['sentence'])
# tfidf = TfidfTransformer()
# X_train_vectorized = vect.transform(csv_mpqa['sentence'])
# clf = MultinomialNB(alpha = 0.1)
# clf.fit(X_train_vectorized, csv_mpqa['subjective'])

text_clf.fit(csv_mpqa['sentence'], csv_mpqa['subjective'])
preds = text_clf.predict(csv_mpqa_validation['sentence'])

print(len(preds))
print(len(csv_mpqa_validation['sentence']))

from sklearn.metrics import accuracy_score
print(accuracy_score(csv_mpqa_validation['subjective'], preds))

from sklearn.model_selection import GridSearchCV

parameters = {
     'vect__ngram_range': [(1, 1), (1, 2)],
     'tfidf__use_idf': (True, False),
     'clf__alpha': (1e-2, 1e-3),
 }

gs_clf = GridSearchCV(text_clf, parameters, cv=5, iid=False, n_jobs=-1)
gs_clf = gs_clf.fit(csv_mpqa['sentence'], csv_mpqa['subjective'])

print(gs_clf.best_score_)

preds = gs_clf.predict(csv_mpqa_validation['sentence'])

from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score
print(recall_score(csv_mpqa_validation['subjective'], preds))
print(precision_score(csv_mpqa_validation['subjective'], preds))
print(f1_score(csv_mpqa_validation['subjective'], preds))

f = open("subjectivity_clues.txt")
clues = f.readlines()
for i in range(len(clues)):
    clue_dict = {}
    for attr in clues[i].split(" "):
        comps = attr.split("=")
        if(len(comps) > 1):
            clue_dict[comps[0]] = comps[1]
    clues[i] = clue_dict

clues = pd.DataFrame(data=clues)

f.close()

print("clues gathered")

def count_clues(text):
    processed = text.replace("-", " ").replace("/", " ")
    processed = ' '.join(processed.split())
    processed = processed.strip()
    processed = processed.replace("\'s", "").replace(" s ", " ")
    processed = ''.join([c for c in processed if c not in punctuation])

    words = processed.split(" ")
    strongsubj = 0
    weaksubj = 0
    for word in words:
        matches = clues.word1[clues.word1 == word.lower()]
        if not matches.empty:
            if clues.iloc[matches.index[0]].type == "strongsubj":
                strongsubj+=1
            elif clues.iloc[matches.index[0]].type == "weaksubj":
                weaksubj += 1
    # print(text + ": " + str(strongsubj) + " : " + str(weaksubj))
    return strongsubj, weaksubj

def label_sentence(sentence):
    current_strong, current_weak = count_clues(sentence)
    # prev_strong, prev_weak = count_clues(sentences[0])
    # next_strong, next_weak = count_clues(sentences[2])
    threshold = 2
    if len(sentence.split()) > 38:
        threshold = 3
    if current_strong >= threshold:
        return 1
    elif current_strong <= (threshold-2):
        return 0
    else:
        return -1

accuracy = 0
precision = 0
total_positives = 0
total = 0
recall = 0
total_true_positives = 0
false_negatives = 0
counter = 0

mean_len_positives = 0
mean_len_negatives = 0

mean_len_true = 0
mean_len_false = 0

total_negatives = 0

total_true = 0
total_false = 0

num_wrong_neg = 0
num_wrong_pos = 0
num_wrong = 0

for i, row in csv_mpqa_validation.iterrows():
    label = label_sentence(row[0])
    if label == preds[i]:
        if label != -1:
            total += 1
            if row[1] == 1:
                total_true += 1
                mean_len_true += len(row[0].split())
                if label == 1:
                    total_true_positives += 1
                    recall += 1
            elif row[1] == 0:
                mean_len_false += len(row[0].split())
                total_false += 1
            if label == 1:
                mean_len_positives += len(row[0].split())
                total_positives += 1
                # print("yuh")
                if label == row[1]:
                    precision += 1
                else:
                    num_wrong_pos += 1
            elif label == 0:
                mean_len_negatives += len(row[0].split())
                total_negatives += 1
                if row[1] == label:
                    false_negatives += 1
                else:
                    num_wrong_neg += 1

            if label == row[1]:
                accuracy += 1
            else:
                num_wrong += 1

print("Naive Bayes Rule Based Hybrid")

print("Precision: " + str(precision/total_positives))
print("Recall: " + str(recall/(total_true_positives + false_negatives)))
print("Accuracy: " + str(accuracy/total))

print("Percent Positives: "+ str(total_positives/total))

print("Percent True: "+ str(total_true/total))

print("Mean Length of Positives: " + str(mean_len_positives/total_positives))
print("Mean Length of Negatives: " + str(mean_len_negatives/total_negatives))

print("Mean Length of True: " + str(mean_len_true/total_true))
print("Mean Length of False: " + str(mean_len_false/total_false))

print("Percent of all Wrong that are Positive: " + str(num_wrong_neg/num_wrong))

print("Percent of all Positive that are wrong: " + str(num_wrong_pos/total_positives))
print("Percent of all Negative that are wrong: "+ str(num_wrong_neg/total_negatives))
