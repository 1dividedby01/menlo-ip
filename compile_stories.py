from string import punctuation
import os
import pandas as pd
import nltk.data

nltk.download('punkt')

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
def split_sentences(text):
  return tokenizer.tokenize(text)

counter = 0
data = []
for filename in os.listdir("stories"):
    if filename.endswith(".story"):
        counter+=1
        f = open("stories/" + filename)
        text = f.read().split("@highlight")[0]
        text = split_sentences(text)
        sentences = []
        for i in range(len(text)):
            if not ('\"' in text[i] or '\'' in text[i] or '(CNN) --' in text[i]) and text[i].strip() != "":
                data.append({"sentence": text[i], "subjective": 0})

        f.close()
        #
        # data += sentences
        if counter % 1000 == 0:
            print(str(counter/90000 * 100) + "% done")

training_data = pd.DataFrame(data=data)
training_data.to_csv('CNN/raw_sentences.csv', index=False)
