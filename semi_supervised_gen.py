import torch
from torch import nn
from torch.utils.data import DataLoader, TensorDataset

import pandas as pd
import numpy as np

from collections import Counter
from string import punctuation

import json

from nltk.tag import pos_tag
from nltk.corpus import words
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

words_set = set(words.words())

ps = PorterStemmer()
lemmatizer = WordNetLemmatizer()

def to_ints(row):
    if not isinstance(row, str):
        return []
    return [vocab_dict[w] for w in row.split() if w in vocab_dict]

def pad_features(sentence):
    if len(sentence) < sequence_length:
        zeros = list(np.zeros(sequence_length-len(sentence)))
        return zeros+list(sentence)
    else:
        return sentence[0:sequence_length]

# csv = pd.read_csv('menlo-ip/training_data.csv')

# csv['subjective'] = np.where(csv['subjective'] == True, 1, 0)

# csv['sentence'] = csv['sentence'].str.lower()
cnn_csv = pd.read_csv('processed.csv')

cnn_csv['subjective'] = np.where(cnn_csv['subjective'] == True, 1, 0)

# cnn_csv['sentence'] = cnn_csv['sentence'].str.lower()

csv = pd.read_csv('mpqa_data.csv')

# csv['sentence'] = csv['sentence'].str.lower()

# should be around 26 for CNN data
sequence_length = 35

# cnn_csv['sentence'] = cnn_csv['sentence'].apply(preprocess)
# csv['sentence'] = csv['sentence'].apply(preprocess)

all_sentences = cnn_csv['sentence'].copy(deep=True)
cnn_csv['sentence_string'] = all_sentences

cat = csv['sentence'].str.cat(sep=' ')

words = cat.split()

counts = Counter(words)

total = len(counts)
sorted_words = counts.most_common(total)

vocab_dict = {w:i+1 for i, (w,c) in enumerate(sorted_words)}
# f = open("csv_vocab_dict", "w")
# f.write(json.dumps(vocab_dict))
# f.close()
# print(vocab_dict)
cnn_csv['sentence'] = cnn_csv['sentence'].apply(to_ints)

# cnn_csv = cnn_csv[len(cnn_csv.sentence) > 4]

cnn_csv['sentence'] = cnn_csv['sentence'].convert_objects(convert_numeric=True)

print(cnn_csv['sentence'].apply(len).describe())
# print(csv['sentence'].convert_objects(convert_numeric=True).head())
cnn_csv = cnn_csv[cnn_csv['sentence'].map(len) > 4]

all_sentences = cnn_csv['sentence_string'].copy(deep=True).tolist()
# print(all_sentences[265])

cnn_csv['sentence'] = cnn_csv['sentence'].apply(pad_features)

train_on_gpu = False
print(len(vocab_dict))

class SubjectivityLSTM(nn.Module):
  def __init__(self, vocab_size, output_size, embedding_dim, hidden_dim, n_layers, drop_prob=0.5):
    super().__init__()

    self.output_size = output_size
    self.n_layers = n_layers
    self.hidden_dim = hidden_dim

    # embedding and LSTM layers
    self.embedding = nn.Embedding(vocab_size, embedding_dim)
    self.lstm = nn.LSTM(embedding_dim, hidden_dim // 2, n_layers,
                       dropout=drop_prob, batch_first=True, bidirectional=True)

    '''
    randomly dropout neurons from the network to increase responsibility among
    individual neurons and reduce dependency on other neurons. Dropout is a
    solution to coadaptation, a major cause of overfitting.
    '''

    # dropout layer
    self.dropout = nn.Dropout(drop_prob)

    # linear and sigmoid layers
    self.fc = nn.Linear(hidden_dim, output_size)
    self.sig = nn.Sigmoid()

  def forward(self, inputs, hidden):
    batch_size = inputs.size(0)
#     print(inputs)
    embeds = self.embedding(inputs)
    lstm_out, hidden = self.lstm(embeds, hidden)

    # contiguous just creates a contiguous copy in memory
    lstm_out = lstm_out.contiguous().view(-1, self.hidden_dim)

    # dropout and fully-connected layer
    out = self.dropout(lstm_out)
    out = self.fc(out)
    # sigmoid function
    sig_out = self.sig(out)

    # reshape to be batch_size first
    sig_out = sig_out.view(batch_size, -1)
    sig_out = sig_out[:, -1] # get last batch of labels

    # return last sigmoid output and hidden state
    return sig_out, hidden

  def init_hidden(self, batch_size):
    weight = next(self.parameters()).data

    if train_on_gpu:
      hidden = (weight.new(self.n_layers * 2, batch_size, self.hidden_dim // 2).zero_().cuda(),
               weight.new(self.n_layers * 2, batch_size, self.hidden_dim // 2).zero_().cuda())
    else:
      hidden = (weight.new(self.n_layers * 2, batch_size, self.hidden_dim // 2).zero_(),
               weight.new(self.n_layers * 2, batch_size, self.hidden_dim // 2).zero_())

    return hidden

def load_model(name):
  path = name + ".pth.tar"
  checkpoint = torch.load(path)
  net.load_state_dict(checkpoint['state_dict'])
  start_batch = checkpoint['batch']
  start_epoch = checkpoint['epoch']

  return start_batch, start_epoch, net

vocab_size = len(vocab_dict) + 1 # the 1 is for the 0 padding
output_size = 1
embedding_dim = 600
hidden_dim = 100
n_layers = 2

net = SubjectivityLSTM(vocab_size, output_size, embedding_dim, hidden_dim, n_layers)

print(net)

start_batch, epoch, net = load_model("model_mpqa_updated")

# have to rerun processing

cnn_X = np.array(list(cnn_csv["sentence"].values))
cnn_X = TensorDataset(torch.tensor(cnn_X), torch.tensor(cnn_csv["subjective"].values))
batch_size_cnn = 1000

cnn_train_loader = DataLoader(cnn_X, shuffle=False, batch_size=batch_size_cnn)
print("data loaded")
cnn_data = []
counter = 0
total = 0

h = net.init_hidden(batch_size_cnn)

net.eval()
for inputs, labels in cnn_train_loader:

  h = tuple([each.data for each in h])

  # get predicted outputs
  inputs = inputs.type(torch.LongTensor)

  if((inputs.shape[0],inputs.shape[1]) != (batch_size_cnn,sequence_length)):
    total += len(output)
    counter += len(output)
    print("doesn't match")
    continue

  output, h = net(inputs, h)

  for index in range(len(output)):
    prediction = output[index]
    if prediction < 0.075 or prediction > 0.925:
      counter += 1
      sentence = inputs[index]
      # print(prediction)
      # maybe this isn't right?
      test_sentence = all_sentences[total]
      test_sentence = to_ints(test_sentence)

      # test_sentence = test_sentence.convert_objects(convert_numeric=True)

      test_sentence = pad_features(test_sentence)
      # print(total)
      # print(index)
      # print("Right Sentence")
      # print(test_sentence)
      # print("Used Sentence")
      # print(sentence)

      cnn_data.append({"sentence": all_sentences[total], "subjective": torch.round(prediction.squeeze()).item()})
    total += 1

  print(str(counter) + "/" + str(total))
  # if total % 200000 == 0:
  #     cnn_training = pd.DataFrame(data=cnn_data)
  #     cnn_training.to_csv('CNN_training.csv', index=False)

cnn_training = pd.DataFrame(data=cnn_data)
cnn_training.to_csv('CNN_training.csv', index=False)
