from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
import pandas as pd
import collections
import operator
import nltk
import numpy as np
import copy

def rightTypes(ngram):
    tags = ngram[2:4]
    for i in range(len(tags)):
        tags[i] = tags[i].split()[0]
    if '-pron-' in ngram or 't' in ngram:
        return False
    # for word in ngram:
    #     if word in stop or word.isspace():
    #         return False
    acceptable_type_pairs = [
        'RB VB', 'RB VBD', 'RB VBG', 'RB VBN', 'RB VBP', 'RB VBZ',
        'JJ NN', 'JJ NNS', 'JJ NNP', 'JJ NNPS', 'JJ PRP', 'JJR NN', 'JJR NNS', 'JJR NNP', 'JJR NNPS', 'JJR PRP', 'JJS NN', 'JJS NNS', 'JJS NNP', 'JJS NNPS', 'JJS PRP',
        'VB NN', 'VB NNS', 'VB NNP', 'VB NNPS', 'VB PRP', 'VBD NN', 'VBD NNS', 'VBD NNP', 'VBD NNPS', 'VBD PRP', 'VBG NN', 'VBG NNS', 'VBG NNP', 'VBG NNPS', 'VBG PRP',
        'VBN NN', 'VBN NNS', 'VBN NNP', 'VBN NNPS', 'VBN PRP', 'VBP NN', 'VBP NNS', 'VBP NNP', 'VBP NNPS', 'VBP PRP', 'VBZ NN', 'VBZ NNS', 'VBZ NNP', 'VBZ NNPS', 'VBZ PRP',
        'VB RB', 'VBD RB', 'VBG RB', 'VBN RB', 'VBP RB', 'VBZ RB',
        'RB JJ'
    ]

    nouns = ['NN','NNS','NNP','NNPS']
    for i in nouns:
        for j in nouns:
            acceptable_type_pairs.append(i + " " + j)

    # print(tags)
    # if "\'" in tags[0][0]:
    #     tags[0][1] = 'PRP$'
    # if "\'" in tags[1][0]:
    #     tags[1][1] = 'PRP$'
    compound = tags[0] + " " + tags[1]
    if compound in acceptable_type_pairs:
        return True
    else:
        return False
#filter bigrams

#function to filter for trigrams
def rightTypesTri(ngram):
    tags = ngram[3:6]
    for i in range(len(tags)):
        tags[i] = tags[i].split()[0]
    # tags = []
    # indices = []
    # for i in range(len(tags_and_indices)):
    #     tags.append(tags_and_indices[i].split()[0])
    #     indices.append(int(tags_and_indices[i].split()[1]))

    # if '-pron-' in ngram or 't' in ngram:
    #     return False
    for w in range(len(ngram)//2):
        word = ngram[w]
        tag = tags[w]
        if tag == "IN":
            continue
        if word.isspace():
            return False

    verbs = ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
    nouns = ['NN','NNS','NNP','NNPS', 'PRP']
    ads = ['RB', 'JJ']

    acceptable_type_pairs = []

    for i in nouns:
        for j in nouns:
            for k in nouns:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in ads:
        for j in ads:
            for k in verbs:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in ads:
        for j in ads:
            for k in nouns:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in ads:
        for j in verbs:
            for k in nouns:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in verbs:
        for j in ads:
            for k in ads:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in nouns:
        for j in nouns:
            acceptable_type_pairs.append(i + ' IN ' + j)

    for i in nouns:
        for j in nouns:
            for k in nouns:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    # print(tags)
    compound = tags[0] + " " + tags[1] + " " + tags[2]
    # print(ngram)
    if compound in acceptable_type_pairs or (tags[0] in verbs and tags[2] in nouns) or (tags[0] == 'JJ' and tags[2] in nouns):

        return True
    else:
        return False
#filter trigrams

# print(filtered_bi)
# print(filtered_tri)
def add_tags(grams, tags):
    g = copy.deepcopy(list(grams))
    for i in range(len(g)):
        # print(list(g[i][0]) + tags[i:i+len(g[i][0])])
        t = copy.deepcopy(tags[i:i+len(g[i][0])])
        for j in range(len(t)):
            t[j] = t[j][1] + " " + str(i+j)
        g[i] = (list(g[i][0]) + t, g[i][1])
        # g[i][0] += tags[i,i+len(g[i][0])]
    # print(g)
    return g

def ab(n):
    if n < -1:
        return -1
    return n

def in_coll(tup, collocations):
    for i in collocations:
        if i[0] == tup[0] and i[1] == tup[1]:
            return True
    return False

def collocate(sentence):
    lemmatized = sentence.split()
    tags = nltk.pos_tag(lemmatized)
    # print(tags)

    tokens = [item for item in lemmatized]

    bigrams = nltk.collocations.BigramAssocMeasures()
    trigrams = nltk.collocations.TrigramAssocMeasures()

    bigram_finder = nltk.collocations.BigramCollocationFinder.from_words(tokens)
    trigram_finder = nltk.collocations.TrigramCollocationFinder.from_words(tokens)

    #bigrams
    bigram_freq = bigram_finder.ngram_fd.items()
    #trigrams
    trigram_freq = trigram_finder.ngram_fd.items()
    # print(trigram_freq)
    bigram_freq = add_tags(bigram_freq, tags)
    trigram_freq = add_tags(trigram_freq, tags)

    filtered_tri = []
    if list(trigram_freq) != []:
        trigram_freq_table = pd.DataFrame(trigram_freq, columns=['trigram','freq']).sort_values(by='freq', ascending=False)
        filtered_tri = trigram_freq_table[trigram_freq_table.trigram.map(lambda x: rightTypesTri(x))]
        filtered_tri = filtered_tri.trigram.tolist()

    filtered_bi = []
    if list(bigram_freq) != []:
        bigram_freq_table = pd.DataFrame(bigram_freq, columns=['bigram','freq']).sort_values(by='freq', ascending=False)
        filtered_bi = bigram_freq_table[bigram_freq_table.bigram.map(lambda x: rightTypes(x))]
        filtered_bi = filtered_bi.bigram.tolist()

    collocations = filtered_bi + filtered_tri

    for co in range(len(collocations)):
        curr = collocations[co]
        collocations[co] = (curr[:len(curr)//2], curr[len(curr)//2:])

    repetitions = False
    all_tags = []
    for c in collocations:
        for t in c[1]:
            if t in all_tags:
                repetitions = True
        all_tags += c[1]
    return collocations

f = open("subjectivity_clues.txt")
clues = f.readlines()
f.close()

strong_subj = []
weak_subj = []

for clue in clues:
    comps = clue.split()
    subjectivity = comps[0].split("=")[1]
    if subjectivity == "strongsubj":
        subjectivity = 3
        strong_subj.append(comps[2].split("=")[1])
    elif subjectivity == "weaksubj":
        weak_subj.append(comps[2].split("=")[1])
        subjectivity = 2

verbs_l = ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
nouns_l = ['NN','NNS','NNP','NNPS', 'PRP']
adverbs_l = ['RB', 'RBS', 'RBR']
adjectives_l = ['JJ', 'JJS', 'JJR']

csv = pd.read_csv("CNN/collocations_data.csv")
mpqa = pd.read_csv("MPQA/mpqa.csv")

mpqa_sentences = {0: [], 1: []}
for i, row in mpqa.iterrows():
    tags = nltk.pos_tag(row.sentence.split())
    new_tags = []
    for j in range(len(tags)):
        if tags[j][1] in adverbs_l:
            new_tags.append("adverb")
        elif tags[j][1] in nouns_l:
            new_tags.append("noun")
        elif tags[j][1] in verbs_l:
            new_tags.append("verb")
        elif tags[j][1] in adjectives_l:
            new_tags.append("adjective")
        elif tags[j][1] == 'MD':
            new_tags.append("modal")
        elif tags[j][1] in ['PRP', 'PRP$']:
            new_tags.append("pronoun")
        else:
            new_tags.append("other")
    if row.subjective == 1 or row.subjective == "1":
        mpqa_sentences[1].append(" ".join(new_tags))
    elif row.subjective == 0 or row.subjective == "0":
        mpqa_sentences[0].append(" ".join(new_tags))

    print(i/len(mpqa))

def get_precisions(pattern):
    tags = pattern
    new_tags = []
    for j in range(len(tags)):
        if tags[j][1] in adverbs_l:
            new_tags.append("adverb")
        elif tags[j][1] in nouns_l:
            new_tags.append("noun")
        elif tags[j][1] in verbs_l:
            new_tags.append("verb")
        elif tags[j][1] in adjectives_l:
            new_tags.append("adjective")
        elif tags[j][1] == 'MD':
            new_tags.append("modal")
        elif tags[j][1] in ['PRP', 'PRP$']:
            new_tags.append("pronoun")
        else:
            new_tags.append("other")
    tags = " ".join(new_tags)

    subjective_precision = 0
    objective_precision = 0

    for p in mpqa_sentences[0]:
        if tags in p:
            subjective_precision += 1

    for p in mpqa_sentences[1]:
        if tags in p:
            objective_precision += 1

    return subjective_precision/(subjective_precision + objective_precision), objective_precision/(subjective_precision + objective_precision)

# columns = "adjective adjective noun,adjective adjective noun noun,adjective adjective verb,adjective adjective verb adverb,adjective adjective verb noun,adjective adjective verb other noun,adjective adjective verb verb noun,adjective adverb noun,adjective adverb noun noun,adjective adverb noun other noun,adjective adverb verb,adjective adverb verb noun,adjective noun,adjective noun noun,adjective noun noun noun,adjective noun noun other noun,adjective noun other noun,adjective other noun,adjective other noun noun,adjective other noun noun noun,adjective other noun other noun,adjective verb noun,adjective verb noun noun,adjn,advadj,adverb adjective,adverb adjective adjective noun,adverb adjective adjective verb,adverb adjective adverb noun,adverb adjective adverb verb,adverb adjective noun,adverb adjective noun noun,adverb adjective other noun,adverb adjective verb,adverb adjective verb noun,adverb adjective verb other noun,adverb adverb noun,adverb adverb noun noun,adverb adverb noun other noun,adverb adverb verb,adverb adverb verb adverb,adverb adverb verb noun,adverb verb,adverb verb adjective adjective,adverb verb adjective adverb,adverb verb adjective noun,adverb verb adverb,adverb verb adverb adjective,adverb verb adverb adverb,adverb verb adverb noun,adverb verb noun,adverb verb noun noun,adverb verb other noun,adverb verb verb noun,nn,noun noun,noun noun noun,noun noun noun noun,noun noun other noun,noun other noun,noun other noun noun,noun other noun noun noun,noun other noun other noun,vadv,verb adjective adjective,verb adjective adjective adjective noun,verb adjective adjective noun,verb adjective adjective other noun,verb adjective adjective verb,verb adjective adverb,verb adjective adverb adjective,verb adjective adverb adverb noun,verb adjective adverb noun,verb adjective adverb verb,verb adjective noun,verb adjective noun noun,verb adverb,verb adverb adjective,verb adverb adjective noun,verb adverb adjective verb,verb adverb adverb,verb adverb adverb adjective,verb adverb adverb noun,verb adverb adverb verb,verb adverb noun,verb adverb noun noun,verb adverb noun other noun,verb adverb verb,verb adverb verb noun,verb noun,verb noun noun,verb noun noun noun,verb noun noun other noun,verb noun other noun,verb other noun,verb other noun noun,verb other noun noun noun,verb other noun other noun,verb verb noun,verb verb noun noun,strongsubj,weaksubj"
# columns = "weaksubj"
# columns = "strongsubj,weaksubj,adjn,advadj,nn,vadv,vn"
# X = csv[columns.split(",")].values.tolist()
# X_test = X[:len(X)//4]
# X_train = X[len(X)//4:]
# y = csv.subjective.values.tolist()
# y_test = y[:len(y)//4]
# y_train = y[len(y)//4:]
X_sentence = csv.sentence.values.tolist()
features = {"strongsubj": [], "weaksubj": [], "adverbs": [], "adjectives": [], "nouns": [], "pronouns": [], "verbs": [], "modals": [], "subjective_precision": [], "objective_precision": []}
for i, row in csv.iterrows():
    strong = 0
    weak = 0
    if i > 100000:
        break
    for j in row.collocation.split():
        if j in strong_subj:
            strong+=1
        if j in weak_subj:
            weak+=1

    adverbs = 0
    adjectives = 0
    nouns = 0
    pronouns = 0
    verbs = 0
    modals = 0
    for j in row.grammar.split():
        if j in adverbs_l:
            adverbs += 1
        if j in adjectives_l:
            adjectives += 1
        if j in nouns_l:
            nouns += 1
        if j == 'MD':
            modals == 1
        if j == 'PRP' or j == 'PRP$':
            pronouns += 1
        if j in verbs_l:
            verbs += 1

    features['strongsubj'].append(strong)
    features['weaksubj'].append(weak)
    features['adverbs'].append(adverbs)
    features['adjectives'].append(adjectives)
    features['nouns'].append(nouns)
    features['pronouns'].append(pronouns)
    features['verbs'].append(verbs)
    features['modals'].append(modals)
    features['subjective_precision'], features['objective_precision'] = get_precisions(row.grammar.split())

print("features collected")

def get_features(rows):
    features_mpqa = {"strongsubj": [], "weaksubj": [], "adverbs": [], "adjectives": [], "nouns": [], "pronouns": [], "verbs": [], "modals": [], "subjective_precision": [], "objective_precision": []}
    mpqa_y = []
    mpqa_sentences = []
    for row in rows:
        strong = 0
        weak = 0
        for j in row['collocation'].split():
            if j in strong_subj:
                strong+=1
            if j in weak_subj:
                weak+=1

        adverbs = 0
        adjectives = 0
        nouns = 0
        pronouns = 0
        verbs = 0
        modals = 0
        for j in row['grammar'].split():
            if j in adverbs_l:
                adverbs += 1
            if j in adjectives_l:
                adjectives += 1
            if j in nouns_l:
                nouns += 1
            if j == 'MD':
                modals == 1
            if j == 'PRP' or j == 'PRP$':
                pronouns += 1
            if j in verbs_l:
                verbs += 1

        features_mpqa['strongsubj'].append(strong)
        features_mpqa['weaksubj'].append(weak)
        features_mpqa['adverbs'].append(adverbs)
        features_mpqa['adjectives'].append(adjectives)
        features_mpqa['nouns'].append(nouns)
        features_mpqa['pronouns'].append(pronouns)
        features_mpqa['verbs'].append(verbs)
        features_mpqa['modals'].append(modals)
        subj_precisions, obj_precisions = get_precisions(row['grammar'].split())
        features_mpqa['subjective_precision'].append(subj_precisions)
        features_mpqa['objective_precision'].append(obj_precisions)
    return features_mpqa

from itertools import combinations

c = [comb for i in range(len(features.keys())) for comb in combinations(features.keys(), i + 1)]
print("starting grid search")
for f in c:
    feats = {}
    for feature in f:
        feats[feature] = features[feature]

    rows = []
    for i in range(len(feats[list(feats.keys())[0]])):
        row = []
        for key in feats.keys():
            row.append(list(feats[key])[i])
        rows.append(row)

    model = KMeans(n_clusters=2, random_state=0).fit(rows)
    sentence_labels = []
    test_sentences = {}
    y_test = []
    for i, row in mpqa.iterrows():
        colls = collocate(row.sentence)
        print(colls)
        colls_dict = []
        for j in colls:
            grammar = ""
            positions = ""
            for k in j[1]:
                grammar += k.split()[0] + " "
                positions += k.split()[1] + " "
            colls_dict.append({"collocation": " ".join(j[0]), "grammar": grammar, "positions": positions})
        features_mpqa = get_features(colls_dict)
        feats = {}
        for feature in f:
            feats[feature] = features_mpqa[feature]

        rows_mpqa = []
        for i in range(len(feats[list(feats.keys())[0]])):
            row_feat = []
            for key in feats.keys():
                row_feat.append(feats[key][i])
            rows_mpqa.append(row_feat)

        labels = model.predict(rows_mpqa)
        if np.sum(labels)/(len(labels)) > 0.5:
            sentence_labels.append(1)
        else:
            sentence_labels.append(0)
        y_test.append(row.subjective)

    print("Results for feature set: " + ",".join(feats.keys()))
    tn, fp, fn, tp = confusion_matrix(y_test, sentence_labels).ravel()
    print("tn: " + str(tn) + " fp: " + str(fp) + " fn: " + str(fn) + " tp: " + str(tp))
    print("accuracy: " + str(accuracy_score(y_test, sentence_labels)))
    print("precision: " + str(precision_score(y_test, sentence_labels)))
    print("recall: " + str(recall_score(y_test, sentence_labels)))
    print("F1: " + str(f1_score(y_test, sentence_labels)))


# model = KMeans(n_clusters=2, random_state=0).fit(X_train)
# labels = model.predict(X_test)

tn, fp, fn, tp = confusion_matrix(y_test, labels).ravel()
print("tn: " + str(tn) + " fp: " + str(fp) + " fn: " + str(fn) + " tp: " + str(tp))
print("accuracy: " + str(accuracy_score(y_test, labels)))
print("precision: " + str(precision_score(y_test, labels)))
print("recall: " + str(recall_score(y_test, labels)))
print("F1: " + str(f1_score(y_test, labels)))
objective_center = model.cluster_centers_[0]
subjective_center = model.cluster_centers_[1]
sorted_features = {}
for i in range(len(objective_center)):
    sorted_features[columns.split(",")[i]] = abs(objective_center[i] - subjective_center[i])
sorted_features = sorted(sorted_features.items(), key=operator.itemgetter(1))
sorted_features = list(collections.OrderedDict(sorted_features).items())
print(sorted_features)
