import numpy as np
import pandas as pd
import re
import nltk
from nltk.corpus import stopwords
import spacy
import string
import copy
import collections
import operator
from string import punctuation

csv= pd.read_csv("MPQA/mpqa.csv")

sentences = csv.sentence
def _removeNonAscii(s): return "".join(i for i in s if ord(i)<128)
sentences = sentences.astype('str')
sentences = sentences.map(lambda x: _removeNonAscii(x))

STOPWORDS_DICT = {lang: set(nltk.corpus.stopwords.words(lang)) for lang in nltk.corpus.stopwords.fileids()}
sentences.drop_duplicates(inplace=True)

nlp = spacy.load('en')

def clean_comments(text):
    #remove punctuations
    regex = re.compile('[' + re.escape(string.punctuation) + '\\r\\t\\n]')
    nopunct = regex.sub(" ", str(text))
    #use spacy to lemmatize comments
    doc = nlp(nopunct, disable=['parser','ner'])
    # lemma = [token.lemma_ for token in doc]
    lemma = [token.text for token in doc]
    return lemma

# lemmatized = sentences.map(clean_comments)
# lemmatized = lemmatized.map(lambda x: [word.lower() for word in x])
# tokens = [item for items in lemmatized for item in items]
# lemmatized = clean_comments("Trump's remark swiftly united a House Democratic caucus that had been torn apart by recent infighting between Pelosi and four freshman women of color.")
# lemmatized = "Trump's remark swiftly united a House Democratic caucus that had been torn apart by recent infighting between Pelosi and four freshman women of color."
# tokens = [item for item in lemmatized.split()]
#
# bigrams = nltk.collocations.BigramAssocMeasures()
# trigrams = nltk.collocations.TrigramAssocMeasures()
#
# bigram_finder = nltk.collocations.BigramCollocationFinder.from_words(tokens)
# trigram_finder = nltk.collocations.TrigramCollocationFinder.from_words(tokens)
#
# #bigrams
# bigram_freq = bigram_finder.ngram_fd.items()
# bigram_freq_table = pd.DataFrame(list(bigram_freq), columns=['bigram','freq']).sort_values(by='freq', ascending=False)
# #trigrams
# trigram_freq = trigram_finder.ngram_fd.items()
# trigram_freq_table = pd.DataFrame(list(trigram_freq), columns=['trigram','freq']).sort_values(by='freq', ascending=False)

stop = set(stopwords.words('english'))

#function to filter for ADJ/NN bigrams
def rightTypes(ngram):
    tags = ngram[2:4]
    for i in range(len(tags)):
        tags[i] = tags[i].split()[0]
    if '-pron-' in ngram or 't' in ngram:
        return False
    # for word in ngram:
    #     if word in stop or word.isspace():
    #         return False
    acceptable_type_pairs = [
        'RB VB', 'RB VBD', 'RB VBG', 'RB VBN', 'RB VBP', 'RB VBZ',
        'JJ NN', 'JJ NNS', 'JJ NNP', 'JJ NNPS', 'JJ PRP', 'JJR NN', 'JJR NNS', 'JJR NNP', 'JJR NNPS', 'JJR PRP', 'JJS NN', 'JJS NNS', 'JJS NNP', 'JJS NNPS', 'JJS PRP',
        'VB NN', 'VB NNS', 'VB NNP', 'VB NNPS', 'VB PRP', 'VBD NN', 'VBD NNS', 'VBD NNP', 'VBD NNPS', 'VBD PRP', 'VBG NN', 'VBG NNS', 'VBG NNP', 'VBG NNPS', 'VBG PRP',
        'VBN NN', 'VBN NNS', 'VBN NNP', 'VBN NNPS', 'VBN PRP', 'VBP NN', 'VBP NNS', 'VBP NNP', 'VBP NNPS', 'VBP PRP', 'VBZ NN', 'VBZ NNS', 'VBZ NNP', 'VBZ NNPS', 'VBZ PRP',
        'VB RB', 'VBD RB', 'VBG RB', 'VBN RB', 'VBP RB', 'VBZ RB',
        'RB JJ'
    ]

    nouns = ['NN','NNS','NNP','NNPS']
    for i in nouns:
        for j in nouns:
            acceptable_type_pairs.append(i + " " + j)

    # print(tags)
    # if "\'" in tags[0][0]:
    #     tags[0][1] = 'PRP$'
    # if "\'" in tags[1][0]:
    #     tags[1][1] = 'PRP$'
    compound = tags[0] + " " + tags[1]
    if compound in acceptable_type_pairs:
        return True
    else:
        return False
#filter bigrams

#function to filter for trigrams
def rightTypesTri(ngram):
    tags = ngram[3:6]
    for i in range(len(tags)):
        tags[i] = tags[i].split()[0]
    # tags = []
    # indices = []
    # for i in range(len(tags_and_indices)):
    #     tags.append(tags_and_indices[i].split()[0])
    #     indices.append(int(tags_and_indices[i].split()[1]))

    # if '-pron-' in ngram or 't' in ngram:
    #     return False
    for w in range(len(ngram)//2):
        word = ngram[w]
        tag = tags[w]
        if tag == "IN":
            continue
        if word.isspace():
            return False

    verbs = ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
    nouns = ['NN','NNS','NNP','NNPS', 'PRP']
    ads = ['RB', 'JJ']

    acceptable_type_pairs = []

    for i in nouns:
        for j in nouns:
            for k in nouns:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in ads:
        for j in ads:
            for k in verbs:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in ads:
        for j in ads:
            for k in nouns:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in ads:
        for j in verbs:
            for k in nouns:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in verbs:
        for j in ads:
            for k in ads:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    for i in nouns:
        for j in nouns:
            acceptable_type_pairs.append(i + ' IN ' + j)

    for i in nouns:
        for j in nouns:
            for k in nouns:
                acceptable_type_pairs.append(i + " " + j + " " + k)

    # print(tags)
    compound = tags[0] + " " + tags[1] + " " + tags[2]
    # print(ngram)
    if compound in acceptable_type_pairs or (tags[0] in verbs and tags[2] in nouns) or (tags[0] == 'JJ' and tags[2] in nouns):

        return True
    else:
        return False
#filter trigrams

# print(filtered_bi)
# print(filtered_tri)
def add_tags(grams, tags):
    g = copy.deepcopy(list(grams))
    for i in range(len(g)):
        # print(list(g[i][0]) + tags[i:i+len(g[i][0])])
        t = copy.deepcopy(tags[i:i+len(g[i][0])])
        for j in range(len(t)):
            t[j] = t[j][1] + " " + str(i+j)
        g[i] = (list(g[i][0]) + t, g[i][1])
        # g[i][0] += tags[i,i+len(g[i][0])]
    # print(g)
    return g

def ab(n):
    if n < -1:
        return -1
    return n

def in_coll(tup, collocations):
    for i in collocations:
        if i[0] == tup[0] and i[1] == tup[1]:
            return True
    return False

def collocate(sentence):
    lemmatized = clean_comments(sentence)
    tags = nltk.pos_tag(lemmatized)
    # print(tags)

    tokens = [item for item in lemmatized]

    bigrams = nltk.collocations.BigramAssocMeasures()
    trigrams = nltk.collocations.TrigramAssocMeasures()

    bigram_finder = nltk.collocations.BigramCollocationFinder.from_words(tokens)
    trigram_finder = nltk.collocations.TrigramCollocationFinder.from_words(tokens)

    #bigrams
    bigram_freq = bigram_finder.ngram_fd.items()
    #trigrams
    trigram_freq = trigram_finder.ngram_fd.items()
    # print(trigram_freq)
    bigram_freq = add_tags(bigram_freq, tags)
    trigram_freq = add_tags(trigram_freq, tags)

    filtered_tri = []
    if list(trigram_freq) != []:
        trigram_freq_table = pd.DataFrame(trigram_freq, columns=['trigram','freq']).sort_values(by='freq', ascending=False)
        filtered_tri = trigram_freq_table[trigram_freq_table.trigram.map(lambda x: rightTypesTri(x))]
        filtered_tri = filtered_tri.trigram.tolist()

    filtered_bi = []
    if list(bigram_freq) != []:
        bigram_freq_table = pd.DataFrame(bigram_freq, columns=['bigram','freq']).sort_values(by='freq', ascending=False)
        filtered_bi = bigram_freq_table[bigram_freq_table.bigram.map(lambda x: rightTypes(x))]
        filtered_bi = filtered_bi.bigram.tolist()

    collocations = filtered_bi + filtered_tri

    for co in range(len(collocations)):
        curr = collocations[co]
        collocations[co] = (curr[:len(curr)//2], curr[len(curr)//2:])

    repetitions = False
    all_tags = []
    for c in collocations:
        for t in c[1]:
            if t in all_tags:
                repetitions = True
        all_tags += c[1]
    # print(collocations)
    # while(repetitions):
    #     collocations_new = []
    #     for ind in range(len(collocations)):
    #         i = collocations[ind]
    #         start_index = int(i[1][0].split()[1])
    #         end_index = int(i[1][len(i[1])-1].split()[1])
    #         should_use = True
    #         for jin in range(0,len(collocations)):
    #             j = collocations[jin]
    #             start_index_b = int(j[1][0].split()[1])
    #             end_index_b = int(j[1][len(j[1])-1].split()[1])
    #
    #             # print(str(start_index) + " " + str(end_index) + " " + str(start_index_b) + " " + str(end_index_b))
    #
    #             if start_index != start_index_b or end_index != end_index_b:
    #                 if start_index >= start_index_b and end_index <= end_index_b:
    #                     if not in_coll(j, collocations_new):
    #                         collocations_new.append(j)
    #                     should_use = False
    #                     break
    #                 if end_index <= end_index_b and end_index >= start_index_b:
    #                     first = i[0]
    #                     first_pos = i[1]
    #                     second = j[0][ab(end_index - start_index_b) + 1:]
    #                     second_pos = j[1][ab(end_index - start_index_b) + 1:]
    #                     if not in_coll((first + second, first_pos + second_pos), collocations_new):
    #                         collocations_new.append((first + second, first_pos + second_pos))
    #                     should_use = False
    #                     break
    #                 if start_index >= start_index_b and start_index <= end_index_b:
    #                     first = j[0]
    #                     first_pos = j[1]
    #                     second = i[0][ab(end_index_b - start_index) + 1:]
    #                     second_pos = i[1][ab(end_index_b - start_index) + 1:]
    #                     if not in_coll((first + second, first_pos + second_pos), collocations_new):
    #                         collocations_new.append((first + second, first_pos + second_pos))
    #                     should_use = False
    #                     break
    #         if should_use and not in_coll(i,collocations_new):
    #             collocations_new.append(i)
    #
    #     collocations = collocations_new

        # repetitions = False
        # all_tags = []
        # for c in collocations:
        #     for t in c[1]:
        #         if t in all_tags:
        #             repetitions = True
        #     all_tags += c[1]
    return collocations

# verbs = ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
# nouns = ['NN','NNS','NNP','NNPS', 'PRP']
# ads = ['RB', 'JJ']
# adverbs = ['RB', 'RBS', 'RBR']
# adjectives = ['JJ', 'JJS', 'JJR']
#
# freq_table_subj = {}
# freq_table_obj = {}
# for k, row in csv.iterrows():
#     if k % 1000 == 0:
#         print(k/len(csv))
#     sentence = row.sentence
#     colls = collocate(sentence)
#     for j in colls:
#         tags = j[1]
#         for i in range(len(tags)):
#             tags[i] = tags[i].split(" ")[0]
#             if tags[i] in verbs:
#                 tags[i] = "verb"
#             elif tags[i] in nouns:
#                 tags[i] = "noun"
#             elif tags[i] not in ads:
#                 tags[i] = "other"
#         tags = " ".join(tags)
#         if row.subjective == 0:
#             if tags in freq_table_obj:
#                 freq_table_obj[tags] += 1
#             else:
#                 freq_table_obj[tags] = 1
#         else:
#             if tags in freq_table_subj:
#                 freq_table_subj[tags] += 1
#             else:
#                 freq_table_subj[tags] = 1
#
# ratios = {}
# subj_length = 0
# obj_length = 0
# for i in freq_table_subj.keys():
#     if i in freq_table_obj:
#         if freq_table_subj[i] > 15 and freq_table_obj[i] > 15:
#             subj_length += freq_table_subj[i]
#             obj_length += freq_table_obj[i]
#             ratios[i] = freq_table_subj[i] / freq_table_obj[i]
#
# freq_table_obj = sorted(freq_table_obj.items(), key=operator.itemgetter(1))
# freq_table_subj = sorted(freq_table_subj.items(), key=operator.itemgetter(1))
#
# freq_table_obj = list(collections.OrderedDict(freq_table_obj).items())
# freq_table_subj = list(collections.OrderedDict(freq_table_subj).items())
#
# print("Objective Table")
# print(freq_table_obj)
# print("Subjective Table")
# print(freq_table_subj)
#
# print("Overall Ratio")
# print(subj_length/obj_length)
#
# freq_table_obj = freq_table_obj[len(freq_table_obj)-15:]
# freq_table_subj = freq_table_subj[len(freq_table_subj)-15:]
#
# # for i in range(len(freq_table_obj)-1, -1, -1):
# #     print(', '.join(map(str, freq_table_obj[i])) + " : " + ', '.join(map(str, freq_table_subj[i])))
#
# ratios = sorted(ratios.items(), key=operator.itemgetter(1))
# ratios = list(collections.OrderedDict(ratios).items())
# for i in range(len(ratios)-1, -1, -1):
#     print(ratios[i])
#
# accuracy = 0
# precision = 0
# total_positives = 0
# total_true = 0
# total = 0
# num_auth = 0
#
# data_dict = []
# all_keys = set()
#
# for k, row in csv.iterrows():
#     if row.subjective != "0" and row.subjective != "1" and row.subjective != 0 and row.subjective != 1:
#         continue
#     sentence = row.sentence
#     if 'said' in sentence or 'say' in sentence or 'explain' in sentence or 'claim' in sentence:
#         num_auth += 1
#         continue
#     collocations = collocate(sentence)
#     rank = []
#     # data = {'AN', 'AV', 'AXN', 'AXV', 'ANX', 'VN', 'VNX', 'NX', ''}
#     data = {'sentence': sentence, 'subjective': row.subjective, 'advadj': 0, 'adjn': 0, 'nn': 0, 'vn': 0, 'vadv': 0}
#     # data = {'sentence': sentence, 'subjective': row.subjective}
#     for coll in collocations:
#         tags = coll[1]
#         for i in range(len(tags)):
#             tags[i] = tags[i].split(" ")[0]
#             if tags[i] in verbs:
#                 tags[i] = "verb"
#             elif tags[i] in nouns:
#                 tags[i] = "noun"
#             elif tags[i] in adverbs:
#                 tags[i] = "adverb"
#             elif tags[i] in adjectives:
#                 tags[i] = "adjective"
#             else:
#                 tags[i] = "other"
#         tags = " ".join(tags)
#         if "adjective noun" in tags:
#             data['adjn'] += 1
#         if "adverb adjective" in tags:
#             data['advadj'] += 1
#         if "noun noun" in tags or "noun other noun" in tags:
#             data['nn'] += 1
#         if "verb noun" in tags or "verb other noun" in tags:
#             data['vn'] += 1
#         if "verb adverb" in tags or "adverb verb" in tags:
#             data['vadv'] += 1
#         if tags in data:
#             data[tags] += 1
#         else:
#             data[tags] = 1
#         # if tags in ['verb other noun noun', 'noun noun noun', 'noun noun noun noun']:
#         #     rank.append(0)
#         # elif tags in ['JJ noun other noun', 'verb JJ noun', 'RB verb other noun', 'RB verb', 'JJ other noun', 'verb noun', 'JJ noun noun noun', 'JJ noun']:
#         #     rank.append(1)
#
#     for key in data.keys():
#         if key == 'sentence' or key == 'subjective':
#             continue
#         data[key] /= round(len(sentence.split()), 4)
#         all_keys.add(key)
#
#     data_dict.append(data)
#
# #     if len(rank) <= 1:
# #         continue
# #     total += 1
# #     rank = sum(rank)/len(rank)
# #     label = 0
# #     if rank > 0.97:
# #         label = 1
# #     if int(row.subjective) == 1:
# #         total_true += 1
# #     if label == 1:
# #         total_positives += 1
# #         if label == int(row.subjective):
# #             precision += 1
# #     # print(str(label) + " " + str(row.subjective))
# #     if label == int(row.subjective):
# #         accuracy += 1
# #
# # print("accuracy: " + str(accuracy/total))
# # print(total_positives)
# # print(total_true)
# # print(total)
# # print("precision: " + str(precision/total_positives))
# # print(num_auth/len(csv))
#
# for i in range(len(data_dict)):
#     for j in all_keys:
#         if j not in data_dict[i]:
#             data_dict[i][j] = 0
#
# training_data = pd.DataFrame(data=data_dict)
# training_data.to_csv('MPQA/collocation_features.csv', index=False)

# print(freq_table_obj.items())
# print(freq_table_subj.items())

# print(collocate("Trumps remark swiftly united a House Democratic caucus that had been torn apart by recent infighting between Pelosi and four freshman women of color."))
# print(collocate("President Trump popped into a wedding reception at his golf club in Bedminster New Jersey on Saturday night prompting bride groom and guests to chant"))
# print(collocate("There has been so much shock recently that it is hard to know what to call it anymore"))
# print(collocate("The president remains wildly unpopular among Christians who are persons of color"))
# print(collocate("i love bananas"))

anns = pd.read_csv("MPQA/mpqa.csv")

data_dict = []
counter = 0
for i, row in anns.iterrows():
    sentence = row.sentence
    sentence = ' '.join(sentence.split())
    processed = sentence.replace("-", " ").replace("/", " ")
    processed = ' '.join(processed.split())
    processed = processed.strip()
    processed = processed.replace("\'s", "").replace(" s ", " ")
    processed = ''.join([c for c in processed if c not in punctuation])
    processed = processed.lower()

    colls = collocate(processed)

    if i % 1000 == 0:
        unsupervised = pd.DataFrame(data_dict)
        print(counter)
        unsupervised.to_csv("CNN/mpqa_collocations.csv", index=False)
    for j in colls:
        counter += 1
        grammar = ""
        positions = ""
        for k in j[1]:
            grammar += k.split()[0] + " "
            positions += k.split()[1] + " "
        data = {"collocation": " ".join(j[0]), "grammar": grammar.strip(), "positions": positions.strip(), "sentence": i}
        # print(data)
        data_dict.append(data)

unsupervised = pd.DataFrame(data_dict)
print(counter)
unsupervised.to_csv("MPQA/mpqa_collocations.csv", index=False)
