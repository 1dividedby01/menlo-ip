import pandas as pd
import numpy as np

f = open("subjectivity_clues.txt")
clues = f.readlines()
f.close()

strong_subj = []
weak_subj = []

for clue in clues:
    comps = clue.split()
    subjectivity = comps[0].split("=")[1]
    if subjectivity == "strongsubj":
        subjectivity = 3
        strong_subj.append(comps[2].split("=")[1])
    elif subjectivity == "weaksubj":
        weak_subj.append(comps[2].split("=")[1])
        subjectivity = 2

anns = pd.read_csv("MPQA/collocation_features.csv")

counter = 0

def preprocess(text):
    global counter
    counter+=1
    if counter % 10000 == 0:
        print(counter)

    comps = text.split()
    strongsubj_count = 0
    weaksubj_count = 0
    for i in range(len(comps)):
        if comps[i] in strong_subj:
            comps[i] = "<strongsubj>"
            strongsubj_count += 1
        elif comps[i] in weak_subj:
            comps[i] = "<weak_subj>"
            weaksubj_count += 1

    return round(strongsubj_count / len(comps), 4), round(weaksubj_count / len(comps), 4)

    processed = ' '.join(comps)

    # for i, ann in anns.iterrows:
    #     tag = "<subjphrase>"
    #     if ann.subjective == 0:
    #         tag = "<objphrase>"
    #     processed.replace(ann.ann, tag)

    return processed

for i, row in anns.iterrows():
    strong, weak = preprocess(row['sentence'])
    anns.loc[i, 'strongsubj'] = strong
    anns.loc[i, 'weaksubj'] = weak

anns.to_csv('MPQA/collocation_features.csv', index=False)

# csv = pd.read_csv("CNN_training.csv")
# csv.sentence = csv.sentence.apply(preprocess)
# csv.to_csv('CNN_training_subjectivity_phrases.csv', index=False)
