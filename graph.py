import matplotlib.pyplot as plt
import pandas as pd

csv = pd.read_csv("CNN/man_anns.csv")

len_freq = {}
all_s = []

for i, row in csv.iterrows():
    all_s.append(len(row.sentence.split()))
    if len(row.sentence.split()) in len_freq:
        len_freq[len(row.sentence.split())] += 1
    else:
        len_freq[len(row.sentence.split())] = 1

keys = len_freq.keys()
keys = sorted(keys)

freq = []

for i in keys:
    freq.append(len_freq[i])


# import numpy as np
# a = np.array(all_s)
# p = np.percentile(a, 88)
# print(p)
#
plt.bar(keys, freq, color='blue')
plt.xlabel("Sentence Length")
plt.ylabel("Frequency")
plt.title("Sentence Lengths from Gold Standard training set")

# plt.xticks(x_pos, x)

plt.show()
