import pandas as pd
import numpy as np

from string import punctuation

from nltk.tag import pos_tag
from nltk.corpus import words
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

words_set = set(words.words())

f = open("subjectivity_clues.txt")
clues = f.readlines()
f.close()

strong_subj = []
weak_subj = []

for clue in clues:
    comps = clue.split()
    subjectivity = comps[0].split("=")[1]
    if subjectivity == "strongsubj":
        subjectivity = 3
        strong_subj.append(comps[2].split("=")[1])
    elif subjectivity == "weaksubj":
        weak_subj.append(comps[2].split("=")[1])
        subjectivity = 2

f = open('sentiwords.txt')
senti = f.readlines()
senti = [s.strip() for s in senti]
f.close()

ps = PorterStemmer()
lemmatizer = WordNetLemmatizer()
counter = 0

def preprocess(text):
    global counter
    counter+=1
    if counter % 10000 == 0:
        print(counter)
    processed = text.replace("-", " ").replace("/", " ")
    processed = ' '.join(processed.split())
    processed = processed.strip()
    processed = processed.replace("\'s", "").replace(" s ", " ")
    processed = ''.join([c for c in processed if c not in punctuation])
    comps = processed.split()
    tags = pos_tag(comps)
    propernouns = [word for word,pos in tags if pos == 'NNP']
    verbs = [word for word,pos in tags if len(pos) == 3 and 'VB' in pos]
    numerals = [word for word,pos in tags if pos == 'CD']
    properadj = [word for word,pos in tags if pos == 'JJ' and word != tags[0][0] and word.lower() != word]
    # print(propernouns)
    # print(verbs)
    for i in range(len(comps)):
        word = comps[i]

        if "$" in word or "£" in word or "€" in word:
            comps[i] = "<money>"
        elif "%" in word or (i+1 < len(comps) and comps[i+1].lower() == "percent"):
            comps[i] = "<percentage>"
        elif word.isdigit() and int(word) >= 1900 and int(word) <= 2075:
            comps[i] = "<year>"
        elif word in numerals:
            comps[i] = "<number>"
        elif word in propernouns:
            comps[i] = "<propernoun>"
        elif word in properadj:
            comps[i] = "<properadj>"
        else:
            comps[i] = comps[i].lower()

            if comps[i] in senti:
                comps[i] = "<senti>"
            elif comps[i] in strong_subj:
                comps[i] = "<strongsubj>"
            elif comps[i] in weak_subj:
                comps[i] = "<weak_subj>"
            else:
                lemm = lemmatizer.lemmatize(comps[i])
                if comps[i] in verbs:
                    comps[i] = lemmatizer.lemmatize(comps[i], pos='v')
                    continue
                if lemm in words_set:
                    comps[i] = lemm
                elif lemm.lower() in words_set:
                    comps[i] = lemm.lower()

    processed = ' '.join(comps)
    return processed.lower()

csv = pd.read_csv('MPQA/mpqa.csv')
print("starting")
csv['sentence'] = csv['sentence'].apply(preprocess)
csv.to_csv('MPQA/mpqa_preprocessed.csv', index=False)

# csv = pd.read_csv('mpqa_ann_validate.csv')
# print("starting")
# csv['ann'] = csv['ann'].apply(preprocess)
# csv.to_csv('MPQA/mpqa_ann_validated_processed.csv', index=False)
