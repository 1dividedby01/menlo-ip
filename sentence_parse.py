import pandas as pd
import nltk
from string import punctuation

anns = pd.read_csv("MPQA/mpqa.csv")

data_dict = []
counter = 0
for i, row in anns.iterrows():
    sentence = row.sentence
    sentence = ' '.join(sentence.split())
    processed = sentence.replace("-", " ").replace("/", " ")
    processed = ' '.join(processed.split())
    processed = processed.strip()
    processed = processed.replace("\'s", "").replace(" s ", " ")
    processed = ''.join([c for c in processed if c not in punctuation])
    processed = processed.lower()
    tags = nltk.pos_tag(processed.split())
    if i % 1000 == 0:
        unsupervised = pd.DataFrame(data_dict)
        print(counter)
        unsupervised.to_csv("MPQA/mpqa_features.csv", index=False)
    counter += 1
    grammar = ""
    text = ""
    for k in tags:
        grammar += k[1] + " "
        text += k[0] + " "
    data = {"collocation": text.strip(), "grammar": grammar.strip(), "sentence": i, "subjective": row.subjective}
    # print(data)
    data_dict.append(data)

unsupervised = pd.DataFrame(data_dict)
print(counter)
unsupervised.to_csv("MPQA/mpqa_features.csv", index=False)
