import os
from pandas import DataFrame
import random
import string

def subj(s):
    if "direct-subjective" in s:
        if "\tintensity=\"low\"" not in s and "\tintensity=\"neutral\"" not in s and "insubstantial" not in s:
            return True
    elif "expressive-subjectivity" in s:
        if "\tintensity=\"low\"" not in s:
            return True
    return False

def obj(s):
    if "objective-speech-event" in s:
        return True

    return False

def find_subj(the_list):
    indices = []
    for i, s in enumerate(the_list):
        if subj(s):
            indices.append(i)
    return indices

def find_obj(the_list):
    indices = []
    for i,s in enumerate(the_list):
        if obj(s):
            indices.append(i)
    return indices

subj_texts = []
obj_texts = []
docs = {}
for file in os.listdir("database.mpqa.3.0/man_anns"):
    # text
    document = ""
    if file == ".DS_Store":
        continue

    annotations = []
    for f in os.listdir("database.mpqa.3.0/man_anns/" + file):
        if f == ".DS_Store":
            continue
        reader = open("database.mpqa.3.0/man_anns/" + file + "/" + f + "/gateman.mpqa.lre.3.0")
        annotations = reader.readlines()
        docs[f] = annotations
        reader.close()

    for f in os.listdir("database.mpqa.3.0/docs/" + file):
        reader = open("database.mpqa.3.0/docs/" + file + "/" + f)
        document = reader.read()
        if f in docs.keys():
            docs[f] = (docs[f], document)
        reader.close()
total = 0
for i in docs.keys():
    annotations = docs[i][0]

    document = docs[i][1]
    annotations = [a for a in annotations if a[0] != "#"]

    subj_anns = find_subj(annotations)
    obj_anns = find_obj(annotations)

    total += len(subj_anns)
    total += len(obj_anns)

    for a in subj_anns:
        ann = annotations[a]
        range = ann.split("\t")[1]
        start_a = range.split(",")[0]
        end_a = range.split(",")[1]
        ann_text = document[int(start_a):int(end_a)+1].strip().replace("\n", "")
        if len(ann_text.split()) > 2:
            subj_texts.append(ann_text)

    for a in obj_anns:
        ann = annotations[a]
        range = ann.split("\t")[1]
        start_a = range.split(",")[0]
        end_a = range.split(",")[1]
        ann_text = document[int(start_a):int(end_a)+1].strip().replace("\n", "")

        if len(ann_text.split()) > 2:
            print(ann_text)
            print(range)
            print(file)
            obj_texts.append(ann_text)
        # print(ann)
        # print(document[int(start_a):int(end_a)+1].strip().replace("\n", ""))

print(len(subj_texts))
print(len(obj_texts))
print(total)

objective = [(ann, 0) for ann in obj_texts]
subjective = [(ann, 1) for ann in subj_texts]

arr = objective + subjective

import pandas as pd
import numpy as np

f = open("subjectivity_clues.txt")
clues = f.readlines()
f.close()

strong_subj = []
weak_subj = []

for clue in clues:
    comps = clue.split()
    subjectivity = comps[0].split("=")[1]
    if subjectivity == "strongsubj":
        subjectivity = 3
        strong_subj.append(comps[2].split("=")[1])
    elif subjectivity == "weaksubj":
        weak_subj.append(comps[2].split("=")[1])
        subjectivity = 2

objective_strong = 0
objective_weak = 0
objective_none = 0
subjective_strong = 0
subjective_weak = 0
subjective_none = 0

for i in arr:
    has_strong = False
    has_weak = False
    for j in strong_subj:
        if j in i[0]:
            has_strong = True

    for j in weak_subj:
        if j in i[0]:
            has_weak = True

    if i[1] == 1 and has_strong:
        subjective_strong += 1
    elif i[1] == 1 and has_weak:
        subjective_weak += 1
    elif i[1] == 1:
        subjective_none += 1
    elif i[1] == 0 and has_strong:
        objective_strong += 1
    elif i[1] == 0 and has_weak:
        objective_weak += 1
    elif i[1] == 0:
        objective_none += 1

print("stats obj")
print(objective_strong/len(objective))
print(objective_weak/len(objective))
print(objective_none)
print("stats subj")
print(subjective_strong/len(subjective))
print(subjective_weak/len(subjective))
print(subjective_none)



# random.shuffle(subjective)
#
#
# random.shuffle(arr)
#
# train = arr[:(len(arr) * 5) // 6]
# validate = arr[(len(arr) * 5) // 6:]
#
# df_train = DataFrame(train, columns = ['ann', 'subjective'])
# df_train.to_csv("mpqa_ann_train.csv", index=False)
#
# df_test = DataFrame(validate, columns = ['ann', 'subjective'])
# df_test.to_csv("mpqa_ann_validate.csv", index=False)
