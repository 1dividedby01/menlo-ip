import pandas as pd
import numpy as np
import nltk

csv_mpqa = pd.read_csv('mpqa_data.csv')
csv_mpqa_validation = pd.read_csv('mpqa_data_validation.csv')

from sklearn.model_selection import train_test_split

# X_train, X_test, y_train, y_test = train_test_split(csv_mpqa['sentence'], csv_mpqa['subjective'], train_size=0.9, random_state=0)
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import roc_auc_score

from sklearn.pipeline import Pipeline

text_clf = Pipeline([
     ('vect', CountVectorizer()),
     ('tfidf', TfidfTransformer()),
     ('clf', MultinomialNB(alpha=1e-3))
 ])

text_clf.fit(csv_mpqa['sentence'], csv_mpqa['subjective'])

from sklearn.metrics import accuracy_score
print(accuracy_score(csv_mpqa_validation['subjective'], preds))

from sklearn.model_selection import GridSearchCV

parameters = {
     'vect__ngram_range': [(1, 1), (1, 2)],
     'tfidf__use_idf': (True, False),
     'clf__alpha': (1e-2, 1e-3),
 }

gs_clf = GridSearchCV(text_clf, parameters, cv=5, iid=False, n_jobs=-1)
gs_clf = gs_clf.fit(csv_mpqa['sentence'], csv_mpqa['subjective'])

print(gs_clf.best_score_)

cnn_data = pd.read_csv('')

preds = gs_clf.predict_proba(csv_mpqa_validation['sentence'])

cnn_training = pd.DataFrame(data=cnn_data)
cnn_training.to_csv('CNN_training.csv', index=False)
