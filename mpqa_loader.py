import os
from pandas import DataFrame
import random
import string
from string import punctuation

def validate(the_list):
    indices = []
    for i, s in enumerate(the_list):
        # if "GATE_direct-subjective" in s:
        #     if "\tintensity=\"low\"" not in s and "\tintensity=\"neutral\"" not in s and "insubstantial" not in s:
        #         indices.append(i)
        # elif "GATE_expressive-subjectivity" in s:
        #     if "\tintensity=\"low\"" not in s:
        #         indices.append(i)
        if "intensity=\"medium\"" in s or "intensity=\"high\"" in s or "intensity=\"extreme\"" in s:
            indices.append(i)
    return indices

subjective_sentences = []
objective_sentences = []

for file in os.listdir("database.mpqa.1.2/man_anns"):
    # text
    documents = []
    if file == ".DS_Store":
        continue
    dat = {}
    # print(len(os.listdir("database.mpqa.1.2/docs/" + file)))
    for f in os.listdir("database.mpqa.1.2/docs/" + file):
        reader = open("database.mpqa.1.2/docs/" + file + "/" + f)
        # documents.append(reader.read())
        dat[f] = {'doc': reader.read(), 'sentences': [], 'anns': []}
        reader.close()

    annotations = []
    sentences = []
    for f in os.listdir("database.mpqa.1.2/man_anns/" + file):
        if f == ".DS_Store":
            continue
        reader = open("database.mpqa.1.2/man_anns/" + file + "/" + f + "/gateman.mpqa.lre.1.2")
        annotations = reader.readlines()
        reader.close()
        annotations = [a for a in annotations if a[0] != "#"]

        reader = open("database.mpqa.1.2/man_anns/" + file + "/" + f + "/gatesentences.mpqa.1.2")
        sentences = reader.readlines()
        reader.close()

        if f in dat:
            dat[f]['sentences'] = sentences
            dat[f]['anns'] = annotations

    for key in dat.keys():
        if dat[f]['sentences'] == [] or dat[f]['anns'] == []:
            continue
        for sentence_range in dat[key]['sentences']:
            comps = sentence_range.split("\t")
            start = comps[1].split(",")[0]
            end = comps[1].split(",")[1]
            document = dat[key]['doc']
            annotations = dat[key]['anns']

            sentence = document[int(start):int(end)+1].strip().replace("\n", "")
            subjective_annotations = validate(annotations)
            is_subjective = False
            for a in subjective_annotations:
                ann = annotations[a]
                range = ann.split("\t")[1]
                start_a = range.split(",")[0]
                end_a = range.split(",")[1]

                if len(document[int(start_a):int(end_a)+1].strip().replace("\n", "").split()) <= 1:
                    continue
                # print(ann)
                # print(document[int(start_a):int(end_a)+1].strip().replace("\n", ""))
                if start_a >= start and end_a <= end:
                    sentence = ' '.join(sentence.split())
                    processed = sentence.replace("-", " ").replace("/", " ")
                    processed = ' '.join(processed.split())
                    processed = processed.strip()
                    processed = processed.replace("\'s", "").replace(" s ", " ")
                    processed = ''.join([c for c in processed if c not in punctuation])
                    processed = processed.lower()
                    subjective_sentences.append(processed)
                    is_subjective = True
                    break

            if not is_subjective:
                sentence = ' '.join(sentence.split())
                processed = sentence.replace("-", " ").replace("/", " ")
                processed = ' '.join(processed.split())
                processed = processed.strip()
                processed = processed.replace("\'s", "").replace(" s ", " ")
                processed = ''.join([c for c in processed if c not in punctuation])
                processed = processed.lower()
                objective_sentences.append(processed)

print(len(objective_sentences))
print(len(subjective_sentences))

objective = [(sentence, 0) for sentence in objective_sentences]
subjective = [(sentence, 1) for sentence in subjective_sentences]
# random.shuffle(subjective)
# half = int(len(subjective)/2)
# subjective = subjective[:half]
# print(len(subjective))

arr = objective + subjective
random.shuffle(arr)

# train = arr[:(len(arr) * 4) // 5]
# validate = arr[(len(arr) * 4) // 5:]

df_train = DataFrame(arr, columns = ['sentence', 'subjective'])
df_train.to_csv("MPQA/mpqa.csv", index=False)

# df_test = DataFrame(validate, columns = ['sentence', 'subjective'])
# df_test.to_csv("mpqa_validate.csv", index=False)
