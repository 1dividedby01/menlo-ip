from string import punctuation

from nltk.tag import pos_tag
from nltk.corpus import words
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

words_set = set(words.words())

f = open("subjectivity_clues.txt")
clues = f.readlines()
f.close()

strong_subj = []
weak_subj = []

for clue in clues:
    comps = clue.split()
    subjectivity = comps[0].split("=")[1]
    if subjectivity == "strongsubj":
        subjectivity = 3
        strong_subj.append(comps[2].split("=")[1])
    elif subjectivity == "weaksubj":
        weak_subj.append(comps[2].split("=")[1])
        subjectivity = 2

ps = PorterStemmer()
lemmatizer = WordNetLemmatizer()

def preprocess(text):
    processed = text.replace("-", " ").replace("/", " ")
    processed = ' '.join(processed.split())
    processed = processed.strip()
    processed = processed.replace("\'s", "").replace(" s ", " ")
    processed = ''.join([c for c in processed if c not in punctuation])
    comps = processed.split()
    tags = pos_tag(comps)
    propernouns = [word for word,pos in tags if pos == 'NNP']
    verbs = [word for word,pos in tags if len(pos) == 3 and 'VB' in pos]
    numerals = [word for word,pos in tags if pos == 'CD']
    properadj = [word for word,pos in tags if pos == 'JJ' and word != tags[0][0] and word.lower() != word]
    # print(propernouns)
    # print(verbs)
    for i in range(len(comps)):
        word = comps[i]

        if "$" in word or "£" in word or "€" in word:
            comps[i] = "<money>"
        elif "%" in word or (i+1 < len(comps) and comps[i+1].lower() == "percent"):
            comps[i] = "<percentage>"
        elif word.isdigit() and int(word) >= 1900 and int(word) <= 2075:
            comps[i] = "<year>"
        elif word in numerals:
            comps[i] = "<number>"
        elif word in propernouns:
            comps[i] = "<propernoun>"
        elif word in properadj:
            comps[i] = "<properadj>"
        else:
            comps[i] = comps[i].lower()
            if comps[i] in strong_subj:
                comps[i] = "<strongsubj>"
            elif comps[i] in weak_subj:
                comps[i] = "<weak_subj>"
            else:
                lemm = lemmatizer.lemmatize(comps[i])
                if comps[i] in verbs:
                    comps[i] = lemmatizer.lemmatize(comps[i], pos='v')
                    continue
                if lemm in words_set:
                    comps[i] = lemm
                elif lemm.lower() in words_set:
                    comps[i] = lemm.lower()

    processed = ' '.join(comps)
    return processed.lower()

import pandas as pd
import numpy as np
import nltk

csv_mpqa = pd.read_csv('MPQA/mpqa_train_preprocessed.csv')
csv_mpqa_validation = pd.read_csv('MPQA/mpqa_validate_preprocessed.csv')

from sklearn.model_selection import train_test_split

# X_train, X_test, y_train, y_test = train_test_split(csv_mpqa['sentence'], csv_mpqa['subjective'], train_size=0.9, random_state=0)
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import roc_auc_score

from sklearn.pipeline import Pipeline

text_clf = Pipeline([
     ('vect', CountVectorizer()),
     ('tfidf', TfidfTransformer()),
     ('clf', MultinomialNB(alpha=1e-3))
 ])

text_clf.fit(csv_mpqa['sentence'], csv_mpqa['subjective'])

# from sklearn.metrics import accuracy_score
# print(accuracy_score(csv_mpqa_validation['subjective'], preds))

from sklearn.model_selection import GridSearchCV

parameters = {
     'vect__ngram_range': [(1, 1), (1, 2)],
     'tfidf__use_idf': (True, False),
     'clf__alpha': (1e-2, 1e-3),
 }

gs_clf = GridSearchCV(text_clf, parameters, cv=5, iid=False, n_jobs=-1)
gs_clf = gs_clf.fit(csv_mpqa['sentence'], csv_mpqa['subjective'])

print(gs_clf.best_score_)

import os
import re
import string
import nltk.data
nltk.download('punkt')

# f = open("subjectivity_clues.txt")
# clues = f.readlines()
# for i in range(len(clues)):
#     clue_dict = {}
#     for attr in clues[i].split(" "):
#         comps = attr.split("=")
#         if(len(comps) > 1):
#             clue_dict[comps[0]] = comps[1]
#     clues[i] = clue_dict
#
# clues = pd.DataFrame(data=clues)
#
# f.close()
#
# print("clues gathered")

data = []
labels = []

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
def split_sentences(text):
  return tokenizer.tokenize(text)

def remove_quotes(text):
    new_text = text
    new_text = re.sub(re.compile('"[^"]*"'), '', new_text)
    return new_text

counter = 0
semi_counter = 0
for filename in os.listdir("stories"):
    if filename.endswith(".story"):
        counter+=1
        f = open("stories/" + filename)
        text = f.read().split("@highlight")[0]
        # text = remove_quotes(text)
        # print(text)
        # sentences = text.split(".")
        text = text.replace(" Mr.", "").replace(" Mrs.", "").replace(" Dr.", "").replace(" Ms.", "")

        # this can be removed if we want punctuation, but it's an unecessary risk rn
        text = text.replace(",", "").replace("\n", " ").replace("\t", " ")
        # clean and compile
        text = split_sentences(text)
        sentences = []
        for i in range(len(text)):
            if not ('\"' in text[i] or '\'' in text[i] or '(CNN) --' in text[i]) and text[i].strip() != "":
                sentence = preprocess(text[i].strip())
                prediction = gs_clf.predict_proba([sentence])[0][1]

                if prediction < 0.012 or prediction > 0.988:
                    semi_counter += 1
                    data.append({"sentence": sentence, "subjective": np.round(prediction)})
                    if semi_counter % 300 == 0:
                        print(sentence)
                        print(prediction)
                    # sentences.append(sentence)
                    # labels.append(np.round(prediction))

        f.close()
        #
        # data += sentences
        if counter % 1000 == 0:
            print(semi_counter)
            print(str(counter/90000 * 100) + "% done")

print("sentences aggregated")

# training_data = []
# for sentence in data:
#     # s = sentence.translate(str.maketrans('','',string.punctuation))
#     words = s.split(" ")
#     count = 0
#     for word in words:
#         matches = clues.word1[clues.word1 == word.lower()]
#         if not matches.empty:
#             if clues.iloc[matches.index[0]].type == "strongsubj":
#                 count+=1
#
#     metadata = {"sentence": sentence, "subjective": False}
#     if count >= 2:
#         metadata["subjective"] = True
#
#     training_data.append(metadata)

training_data = pd.DataFrame(data=data)
training_data.to_csv('training_data.csv', index=False)
# print(training_data.index[training_data.subjective == False])
