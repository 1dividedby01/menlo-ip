import pandas as pd
import os
import re
import string
from string import punctuation
import nltk
import nltk.data
import random
from sklearn.metrics import confusion_matrix

def validate(the_list):
    indices = []
    for i, s in enumerate(the_list):
        if "intensity=\"medium\"" in s or "intensity=\"high\"" in s or "intensity=\"extreme\"" in s:
            indices.append(i)
    return indices

sentences_mpqa = []

for file in os.listdir("database.mpqa.1.2/man_anns"):
    sentences_sub = []
    # text
    document = ""
    if file == ".DS_Store":
        continue
    for f in os.listdir("database.mpqa.1.2/docs/" + file):
        reader = open("database.mpqa.1.2/docs/" + file + "/" + f)
        document = reader.read()
        reader.close()

    annotations = []
    sentences = []
    for f in os.listdir("database.mpqa.1.2/man_anns/" + file):
        if f == ".DS_Store":
            continue
        reader = open("database.mpqa.1.2/man_anns/" + file + "/" + f + "/gateman.mpqa.lre.1.2")
        annotations = reader.readlines()
        reader.close()

        reader = open("database.mpqa.1.2/man_anns/" + file + "/" + f + "/gatesentences.mpqa.1.2")
        sentences = reader.readlines()
        reader.close()

    annotations = [a for a in annotations if a[0] != "#"]

    for sentence_range in sentences:
        comps = sentence_range.split("\t")
        start = comps[1].split(",")[0]
        end = comps[1].split(",")[1]

        sentence = document[int(start):int(end)+1].strip().replace("\n", "")
        subjective_annotations = validate(annotations)
        is_subjective = False
        for a in subjective_annotations:
            ann = annotations[a]
            range_a = ann.split("\t")[1]
            start_a = range_a.split(",")[0]
            end_a = range_a.split(",")[1]
            if start_a >= start and end_a <= end:
                sentence = ' '.join(sentence.split())
                sentences_sub.append((sentence, 1))
                is_subjective = True
                break

        if not is_subjective:
            sentence = ' '.join(sentence.split())
            sentences_sub.append((sentence, 0))
    sentences_mpqa.append(sentences_sub)

nltk.download('punkt')

f = open("subjectivity_clues.txt")
clues = f.readlines()
for i in range(len(clues)):
    clue_dict = {}
    for attr in clues[i].split(" "):
        comps = attr.split("=")
        if(len(comps) > 1):
            clue_dict[comps[0]] = comps[1]
    clues[i] = clue_dict

clues = pd.DataFrame(data=clues)

f.close()

print("clues gathered")

data = []

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

def count_clues(text):
    processed = text.replace("-", " ").replace("/", " ")
    processed = ' '.join(processed.split())
    processed = processed.strip()
    processed = processed.replace("\'s", "").replace(" s ", " ")
    processed = ''.join([c for c in processed if c not in punctuation])

    words = processed.split(" ")
    strongsubj = 0
    weaksubj = 0
    for word in words:
        matches = clues.word1[clues.word1 == word.lower()]
        if not matches.empty:
            if clues.iloc[matches.index[0]].type == "strongsubj":
                strongsubj+=1
            elif clues.iloc[matches.index[0]].type == "weaksubj":
                weaksubj += 1
    # print(text + ": " + str(strongsubj) + " : " + str(weaksubj))
    return strongsubj, weaksubj

def label_sentence(sentences):
    current_strong, current_weak = count_clues(sentences[1])
    prev_strong, prev_weak = count_clues(sentences[0])
    next_strong, next_weak = count_clues(sentences[2])
    threshold = 2
    # if len(sentences[1].split()) > 38:
    #     threshold = 3
    # if current_strong >= threshold or current_weak >= 4:
    if current_strong >= threshold or current_weak >= 4:
        return 1
    # elif current_strong <= (threshold-1) and prev_strong + next_strong <= 1 and prev_weak + current_weak + next_weak <= 2:
    elif current_strong == 0 and prev_strong + next_strong <= 1 and prev_weak + current_weak + next_weak <= 2:
        return 0
    else:
        return -1

csv = pd.read_csv('CNN/man_anns.csv')
accuracy = 0
precision = 0
total_positives = 0
total = 0
recall = 0
total_true_positives = 0
false_negatives = 0
counter = 0

mean_len_positives = 0
mean_len_negatives = 0

mean_len_true = 0
mean_len_false = 0

total_negatives = 0

total_true = 0
total_false = 0

num_wrong_neg = 0
num_wrong_pos = 0
num_wrong = 0

four = 0

# man_anns = pd.read_csv("MPQA/mpqa.csv")
# num_auth = 0
# all_sentences = 0
# for i, row in man_anns.iterrows():
#     all_sentences += 1
#     # print(row.subjective)
#     # if 'said' in row.sentence or 'say' in row.sentence or 'explain' in row.sentence or 'claim' in row.sentence:
#     #     num_auth += 1
#     #     # print(row)
#     #     continue
#     if row.subjective != "1" and row.subjective != "0" and row.subjective != 1 and row.subjective != 0:
#         print(row)
#         continue
#     true_label = int(row[1])
#     label = label_sentence([row[0], row[0], row[0]])
#
#     counter += 1
#     if counter % 1000 == 0:
#         print(counter)
#
#     # if label == 0:
#     #     rand = random.uniform(0, 1)
#     #     if rand > 0.2:
#     #         continue
#     # print(row.sentence)
#     # print(row.subjective)
#     if label != -1:
#         total += 1
#         if true_label == 1:
#             total_true += 1
#             mean_len_true += len(row[0].split())
#             if label == 1:
#                 total_true_positives += 1
#                 recall += 1
#         elif true_label == 0:
#             mean_len_false += len(row[0].split())
#             total_false += 1
#         if label == 1:
#             mean_len_positives += len(row[0].split())
#             total_positives += 1
#             # print("yuh")
#             if label == true_label:
#                 precision += 1
#             else:
#                 num_wrong_pos += 1
#         elif label == 0:
#             mean_len_negatives += len(row[0].split())
#             total_negatives += 1
#             if true_label == label:
#                 false_negatives += 1
#             else:
#                 num_wrong_neg += 1
#
#         if label == true_label:
#             accuracy += 1
#         else:
#             num_wrong += 1
predictions = []
true = []
for story_mpqa in sentences_mpqa:
    for i in range(1, len(story_mpqa)-1):
        row = story_mpqa[i]
        label = label_sentence([story_mpqa[i-1][0], story_mpqa[i][0], story_mpqa[i+1][0]])
        predictions.append(label)
        true.append(int(row[1]))
        counter += 1
        if counter % 1000 == 0:
            print(counter)

        # if label == 1:
        #     rand = random.uniform(0, 1)
        #     if rand > 0.24:
        #         continue
        # print(row.sentence)
        # print(row.subjective)
        if label != -1:
            total += 1
            if row[1] == 1:
                total_true += 1
                mean_len_true += len(row[0].split())
                if label == 1:
                    total_true_positives += 1
                    recall += 1
            elif row[1] == 0:
                mean_len_false += len(row[0].split())
                total_false += 1
            if label == 1:
                mean_len_positives += len(row[0].split())
                total_positives += 1
                # print("yuh")
                if label == row[1]:
                    precision += 1
                else:
                    num_wrong_pos += 1
            elif label == 0:
                mean_len_negatives += len(row[0].split())
                total_negatives += 1
                if row[1] == label:
                    false_negatives += 1
                else:
                    num_wrong_neg += 1

            if label == row[1]:
                accuracy += 1
            else:
                num_wrong += 1

print(total_true)
print(total_false)

print(confusion_matrix(true, predictions))

# print("Num Sentences with different author: "+ str(num_auth/all_sentences))

print("Precision: " + str(precision/total_positives))
print("Recall: " + str(recall/(total_true_positives + false_negatives)))
print("Accuracy: " + str(accuracy/total))

print("Percent Positives: "+ str(total_positives/total))

print("Percent True: "+ str(total_true/total))

print("Mean Length of Positives: " + str(mean_len_positives/total_positives))
print("Mean Length of Negatives: " + str(mean_len_negatives/total_negatives))

print("Mean Length of True: " + str(mean_len_true/total_true))
print("Mean Length of False: " + str(mean_len_false/total_false))

print("Percent of all Wrong that are Positive: " + str(num_wrong_pos/num_wrong))

print("Percent of all Positive that are wrong: " + str(num_wrong_pos/total_positives))
print("Percent of all Negative that are wrong: "+ str(num_wrong_neg/total_negatives))
